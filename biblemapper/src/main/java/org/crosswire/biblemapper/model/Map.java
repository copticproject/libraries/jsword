/**
 * Distribution License:
 * JSword is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, version 2 as published by
 * the Free Software Foundation. This program is distributed in the hope
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * The License is available on the internet at:
 *       http://www.gnu.org/copyleft/gpl.html
 * or by writing to:
 *      Free Software Foundation, Inc.
 *      59 Temple Place - Suite 330
 *      Boston, MA 02111-1307, USA
 *
 * Copyright: 2005
 *     The copyright to this program is held by it's authors.
 *
 * ID: $Id$
 */
package org.crosswire.biblemapper.model;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.util.Iterator;
import java.util.List;

import javax.swing.event.EventListenerList;

import org.crosswire.jsword.book.basic.AbstractPassageBook;
import org.crosswire.jsword.versification.BibleBook;
import org.crosswire.jsword.versification.Versification;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A map is an array of Nodes (verses with position).
 * 
 * @see gnu.gpl.License for license details.<br>
 *      The copyright to this program is held by it's authors.
 * @author Joe Walker [joe at eireneh dot com]
 */
public class Map implements Serializable {
    /**
     * Basic constructor
     */
    public Map(AbstractPassageBook bible, int dimensions) {
        this.bible = bible;
        this.dimensions = dimensions;

        Versification v11n = this.bible.getVersification();
        // Create the array of Nodes
        int bie = v11n.getBookCount();
        this.nodes = new Position[bie + 1][];
        for (int b = 1; b <= bie; b++) {
            BibleBook bb = v11n.getBook(b);
            int cib = v11n.getLastChapter(bb);
            nodes[b] = new Position[cib + 1];
            for (int c = 1; c <= cib; c++) {
                float[] pos = new float[dimensions];

                for (int d = 0; d < dimensions; d++) {
                    pos[d] = 0.0f;
                }

                nodes[b][c] = new Position(pos);
            }
        }
    }

    public AbstractPassageBook getBible() {
        return bible;
    }

    /**
     * Get the number of dimensions in the nodes in this map
     * 
     * @return The number of dimensions
     */
    public int getDimensions() {
        return dimensions;
    }

    /**
     * Get the position (as a float array) of a node by the ordinal number of
     * the verse that it contains
     * 
     * @return The requested node position
     */
    public float[] getPositionArrayCopy(int book, int chapter) {
        try {
            return (float[]) nodes[book][chapter].pos.clone();
        } catch (ArrayIndexOutOfBoundsException ex) {
            log.error("getPosition() book=" + book + " chapter=" + chapter, ex);
            return new float[] {
                    0.0f, 0.0f
            };
        }
    }

    /**
     * Get the position (as a float array) of a node by the ordinal number of
     * the verse that it contains
     * 
     * @param idx
     *            The index into the position array for the given verse
     * @return The requested node position
     */
    public float getPositionDimension(BibleBook book, int chapter, int idx) {
        int b = bible.getVersification().getOrdinal(book);
        try {
            return nodes[b][chapter].pos[idx];
        } catch (ArrayIndexOutOfBoundsException ex) {
            log.error("getPositionDimension() book=" + book + " chapter=" + chapter + " dim=" + idx, ex);
            return 0.0f;
        }
    }

    /**
     * Get the position of a node by the ordinal number of the verse that it
     * contains
     */
    public void setPosition(BibleBook book, int chapter, float[] pos) {
        int b = bible.getVersification().getOrdinal(book);
        nodes[b][chapter].pos = pos;

        fireMapChanged(book, chapter);
    }

    /**
     * Get the position of a node by the ordinal number of the verse that it
     * contains
     * 
     * @param idx
     *            The index into the position array for the given verse
     * @param f
     *            The new position
     */
    public void setPositionDimension(BibleBook book, int chapter, int idx, float f) {
        int b = bible.getVersification().getOrdinal(book);
        nodes[b][chapter].pos[idx] = f;

        fireMapChanged(book, chapter);
    }

    /**
     * Fix the layout to a fairly random one
     */
    public void setLayoutRandom() {
        Versification v11n = this.bible.getVersification();

        for (int b = 1; b <= v11n.getBookCount(); b++) {
            BibleBook bb = v11n.getBook(b);
            for (int c = 1; c <= v11n.getLastChapter(bb); c++) {
                nodes[b][c] = new Position(new float[] {
                        (float) Math.random(), (float) Math.random()
                });
            }
        }
    }

    /**
     * Fix the layout to a simple book/chapter line default
     */
    public void setLayoutSimple() {
        if (dimensions != 2) {
            throw new IllegalArgumentException("Can't set simple layout for maps with " + dimensions + " dimensions.");
        }

        Versification v11n = this.bible.getVersification();

        float start = 0.05F;
        float end = 0.95F;
        float mid = (end - start) / 2;
        float scale = end - start;

        int bie = v11n.getBookCount();
        for (int b = 1; b <= bie; b++) {
            BibleBook bb = v11n.getBook(b);
            float y = (((float) (b - 1)) / (bie - 1)) * scale + start;

            int cib = v11n.getLastChapter(bb);
            if (cib == 1) {
                nodes[b][1] = new Position(new float[] {
                        mid + start, y
                });
            } else {
                for (int c = 1; c <= cib; c++) {
                    float x = (((float) (c - 1)) / (cib - 1)) * scale + start;

                    nodes[b][c] = new Position(new float[] {
                            x, y
                    });
                }
            }
        }

        fireMapRewritten();
    }

    /**
     * Apply the rules to the map.
     * 
     * @param rules
     *            The rules to apply
     */
    public void applyRules(Rule[] rules) {
        Versification v11n = this.bible.getVersification();
        // For each verse
        for (int b = 1; b <= v11n.getBookCount(); b++) {
            BibleBook bb = v11n.getBook(b);
            for (int c = 1; c <= v11n.getLastChapter(bb); c++) {
                Position[][] dar = new Position[rules.length][];
                for (int j = 0; j < rules.length; j++) {
                    dar[j] = rules[j].getScaledPosition(this, bb, c);

                    /*
                     * if (log.isDebugEnabled()) {
                     * log.debug("Rule: "+j+" ("+
                     * rules[j].getClass().getName(
                     * )+") scale="+rules[j].getScale()); StringBuilder out =
                     * new StringBuilder(" "); for (int i=0; i<dar[j].length;
                     * i++) {
                     * out.append(" ("+i+"="+dar[j][i].pos[0]+","+dar[
                     * j][i].pos[1]+")"); } log.debug(out); }
                     */
                }

                Position[] total = cat(dar);
                nodes[b][c] = PositionUtil.average(total, dimensions);
                // log.debug("Total:");
                // log.debug("  (t="+nodes[b][c].pos[0]+","+nodes[b][c].pos[1]+")");
            }
        }

        fireMapRewritten();
    }

    /**
     * Fix the layout to a fairly random one
     */
    public void debug(PrintWriter out) {
        Versification v11n = this.bible.getVersification();
        for (int b = 1; b <= v11n.getBookCount(); b++) {
            BibleBook bb = v11n.getBook(b);
            log.debug("Book " + b);
            for (int c = 1; c <= v11n.getLastChapter(bb); c++) {
                log.debug("  Chapter " + c + ": Position=(" + nodes[b][c].pos[0] + "," + nodes[b][c].pos[1] + ")");
            }
        }
    }

    /**
     * Add a map listener to the list of things wanting to know whenever we make
     * some changes to the map
     */
    public void addMapListener(MapListener li) {
        listeners.add(MapListener.class, li);
    }

    /**
     * Remove a progress listener from the list of things wanting to know
     * whenever we make some progress
     */
    public void removeMapListener(MapListener li) {
        listeners.remove(MapListener.class, li);
    }

    /**
     * Before we save/load something to/from disk we want to ensure that we
     * don't loose the list of things that have registered to recieve map change
     * events.
     */
    public EventListenerList getEventListenerList() {
        return listeners;
    }

    /**
     * Before we save/load something to/from disk we want to ensure that we
     * don't loose the list of things that have registered to recieve map change
     * events.
     */
    public void setEventListenerList(EventListenerList listeners) {
        this.listeners = listeners;
    }

    /**
     * What is the average position of all the nodes in this map
     * 
     * @return The center of gravity
     */
    public Position getCenterOfGravity() {
        // to cheat ...
        // return new Position(new float[] { 0.5F, 0.5F });

        if (cog == null || replies > MAX_REPLIES) {
            cog = PositionUtil.average(nodes, dimensions);
            replies = 0;
        }

        replies++;
        return cog;
    }

    /**
     * Called to fire a MapEvent to all the Listeners, when a single node has
     * changed position.
     */
    protected void fireMapChanged(BibleBook book, int chapter) {
        // Guaranteed to return a non-null array
        Object[] contents = listeners.getListenerList();

        // Process the listeners last to first, notifying
        // those that are interested in this event
        MapEvent ev = null;
        for (int i = contents.length - 2; i >= 0; i -= 2) {
            if (contents[i] == MapListener.class) {
                if (ev == null) {
                    ev = new MapEvent(bible, this, book, chapter);
                }

                ((MapListener) contents[i + 1]).mapChanged(ev);
            }
        }
    }

    /**
     * Called to fire a MapEvent to all the Listeners, when a single node has
     * changed position.
     */
    protected void fireMapRewritten() {
        // Guaranteed to return a non-null array
        Object[] contents = listeners.getListenerList();

        // Process the listeners last to first, notifying
        // those that are interested in this event
        MapEvent ev = null;
        for (int i = contents.length - 2; i >= 0; i -= 2) {
            if (contents[i] == MapListener.class) {
                if (ev == null) {
                    ev = new MapEvent(this);
                }

                ((MapListener) contents[i + 1]).mapRewritten(ev);
            }
        }
    }

    /**
     * Take an array of Position arrays can cat them all together to make a
     * single array containing all of them.
     * 
     * @param dar
     *            The array of Position arrays
     * @return The single big array
     */
    public static Position[] cat(Position[][] dar) {
        int size = 0;
        for (int i = 0; i < dar.length; i++) {
            size += dar[i].length;
        }

        Position[] total = new Position[size];

        int offset = 0;
        for (int i = 0; i < dar.length; i++) {
            System.arraycopy(dar[i], 0, total, offset, dar[i].length);
            offset += dar[i].length;
        }

        return total;
    }

    /**
     * Save link data to XML as a stream.
     */
    public void load(Reader out) throws IOException {
        try {
            SAXBuilder builder = new SAXBuilder();
            Document doc = builder.build(out);
            Element root = doc.getRootElement();
            fromXML(root);
        } catch (JDOMException ex) {
            throw new IOException(ex.getMessage());
        }
    }

    /**
     * Save link data to XML as a stream.
     */
    public void save(Writer out) throws IOException {
        Element root = toXML();
        Document doc = new Document(root);
        XMLOutputter outputter = new XMLOutputter(Format.getPrettyFormat());
        outputter.output(doc, out);
    }

    /**
     * Generate links from an XML representation.
     * 
     * @param epos
     *            The root 'links' element
     */
    public void fromXML(Element epos) throws JDOMException {
        if (!epos.getName().equals("positions")) {
            throw new JDOMException("root element is not called 'links'");
        }

        dimensions = Integer.parseInt(epos.getAttributeValue("dimensions"));

        List ebs = epos.getChildren("book");
        Iterator bit = ebs.iterator();
        while (bit.hasNext()) {
            Element eb = (Element) bit.next();
            int b = Integer.parseInt(eb.getAttributeValue("num"));

            List ecs = eb.getChildren("chapter");
            Iterator cit = ecs.iterator();
            while (cit.hasNext()) {
                Element ec = (Element) cit.next();
                int c = Integer.parseInt(ec.getAttributeValue("num"));

                float[] fa = new float[dimensions];
                for (int d = 0; d < dimensions; d++) {
                    fa[d] = Float.parseFloat(ec.getAttributeValue("dim" + d));
                }

                nodes[b][c] = new Position(fa);
            }
        }
    }

    /**
     * Save link data to XML as a JDOM tree.
     */
    public Element toXML() {
        Versification v11n = this.bible.getVersification();
        Element epos = new Element("positions");
        epos.setAttribute("dimensions", "" + dimensions);

        for (int b = 1; b <= v11n.getBookCount(); b++) {
            BibleBook bb = v11n.getBook(b);
            Element eb = new Element("book");
            eb.setAttribute("num", "" + b);
            eb.setAttribute("name", v11n.getPreferredName(bb));
            epos.addContent(eb);

            for (int c = 1; c <= v11n.getLastChapter(bb); c++) {
                Position node = nodes[b][c];
                Element ec = new Element("chapter");
                ec.setAttribute("num", "" + c);

                for (int d = 0; d < dimensions; d++) {
                    ec.setAttribute("dim" + d, "" + node.pos[d]);
                }
                eb.addContent(ec);
            }
        }

        return epos;
    }

    /**
     * Initialize the transient fields
     * 
     * @param in
     *            The stream to read our state from
     */
    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        in.defaultReadObject();
        listeners = new EventListenerList();
    }

    /**
     * The log stream
     */
    private static final Logger log = LoggerFactory.getLogger(LinkArray.class);

    /**
     * What is the maximum calculations between re-calcing the CoG
     */
    private static final int MAX_REPLIES = 1;

    /**
     * The current center of gravity
     */
    private Position cog = null;

    /**
     * How long until we next calculate the center of gravity
     */
    private int replies = 0;

    /**
     * The array of verse nodes
     */
    private Position[][] nodes;

    /** The Bible being mapped */
    private AbstractPassageBook bible;

    /**
     * The number of dimensions in the display
     */
    private int dimensions;

    /**
     * The number of links that we track for a node
     */
    public static final int LINKS_PER_NODE = 20;

    /**
     * The list of listeners
     */
    protected transient EventListenerList listeners = new EventListenerList();

    /**
     * Serialization ID - a serialization of nodes and dimensions
     */
    static final long serialVersionUID = -193572391252539071L;
}
