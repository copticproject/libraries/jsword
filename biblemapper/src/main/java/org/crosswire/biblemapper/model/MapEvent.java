/**
 * Distribution License:
 * JSword is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, version 2 as published by
 * the Free Software Foundation. This program is distributed in the hope
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * The License is available on the internet at:
 *       http://www.gnu.org/copyleft/gpl.html
 * or by writing to:
 *      Free Software Foundation, Inc.
 *      59 Temple Place - Suite 330
 *      Boston, MA 02111-1307, USA
 *
 * Copyright: 2005
 *     The copyright to this program is held by it's authors.
 *
 * ID: $Id$
 */
package org.crosswire.biblemapper.model;

import java.util.EventObject;

import org.crosswire.jsword.book.basic.AbstractPassageBook;
import org.crosswire.jsword.versification.BibleBook;

/**
 * A MapEvent happens whenever a Map changes.
 * 
 * @see gnu.gpl.License for license details.<br>
 *      The copyright to this program is held by it's authors.
 * @author Joe Walker [joe at eireneh dot com]
 */
public class MapEvent extends EventObject {
    public MapEvent(Map source) {
        super(source);
    }
    /**
     * Initialize a MapEvent
     * 
     * @param source
     *            The map that started this off
     */
    public MapEvent(AbstractPassageBook bible, Map source, BibleBook book, int chapter) {
        super(source);

        if (chapter < 1 || chapter > bible.getVersification().getLastChapter(book)) {
            throw new IllegalArgumentException("Invalid chapter");
        }
        this.bible = bible;
        this.book = book;
        this.chapter = chapter;
    }

    /**
     * Get the Bible.
     * @return the Bible
     */
    public AbstractPassageBook getBible() {
        return bible;
    }

    /**
     * Get the verse ordinal that changed position or null if the whole table
     * changed
     * 
     * @return The progress
     */
    public BibleBook getChangedBook() {
        return book;
    }

    /**
     * Get the verse ordinal that changed position or null if the whole table
     * changed
     * 
     * @return The progress
     */
    public int getChangedChapter() {
        return chapter;
    }

    /** The Bible */
    private AbstractPassageBook bible;

    /**
     * The book number
     */
    private BibleBook book;

    /**
     * The chapter number
     */
    private int chapter;

    /**
     * Serialization ID
     */
    private static final long serialVersionUID = 3258129146210301749L;
}
