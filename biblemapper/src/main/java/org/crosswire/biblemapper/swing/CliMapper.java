/**
 * Distribution License:
 * JSword is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, version 2 as published by
 * the Free Software Foundation. This program is distributed in the hope
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * The License is available on the internet at:
 *       http://www.gnu.org/copyleft/gpl.html
 * or by writing to:
 *      Free Software Foundation, Inc.
 *      59 Temple Place - Suite 330
 *      Boston, MA 02111-1307, USA
 *
 * Copyright: 2005
 *     The copyright to this program is held by it's authors.
 *
 * ID: $Id$
 */
package org.crosswire.biblemapper.swing;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.Iterator;

import org.crosswire.jsword.book.Book;
import org.crosswire.jsword.book.BookData;
import org.crosswire.jsword.book.BookMetaData;
import org.crosswire.jsword.book.Books;
import org.crosswire.jsword.book.OSISUtil;
import org.crosswire.jsword.passage.Key;
import org.crosswire.jsword.passage.PassageTally;
import org.crosswire.jsword.passage.Verse;
import org.crosswire.jsword.passage.VerseRange;
import org.crosswire.jsword.versification.BibleBook;
import org.crosswire.jsword.versification.Versification;
import org.crosswire.jsword.versification.system.Versifications;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

/**
 * Command line mapping tool.
 * 
 * @see gnu.gpl.License for license details.<br>
 *      The copyright to this program is held by it's authors.
 * @author Joe Walker [joe at eireneh dot com]
 */
public class CliMapper {
    /**
     * Start point
     */
    public static void main(String[] args) {
        new CliMapper().run();
    }

    /**
     * Create a new Map
     */
    public void run() {
        try {
            PrintWriter dbout = new PrintWriter(new FileOutputStream("c:\\database.csv"));
            PrintWriter xlout = new PrintWriter(new FileOutputStream("c:\\sheet.csv"));

            Book book = Books.installed().getBook("KJV");
            v11n = Versifications.instance().getVersification(book.getBookMetaData().getProperty(BookMetaData.KEY_VERSIFICATION));
            // Matcher engine = new Matcher(bible);

            Element links = new Element("links");

            for (int i = 1; i <= v11n.getBookCount(); i++) {
                BibleBook bb = v11n.getBook(i);

                Element eb = new Element("book");
                eb.setAttribute("num", "" +i );
                eb.setAttribute("name", v11n.getBookName(bb).getPreferredName());
                links.addContent(eb);

                int chff = v11n.getLastChapter(bb);
                int vsff = v11n.getLastVerse(bb, chff);
                Verse start = new Verse(v11n, bb, 1, 1);
                Verse end = new Verse(v11n, bb, chff, vsff);
                VerseRange range = new VerseRange(v11n, start, end);

                for (int c = 1; c <= chff; c++) {
                    Element ec = new Element("chapter");
                    ec.setAttribute("num", "" + c);
                    eb.addContent(ec);

                    PassageTally total = new PassageTally(v11n);
                    total.setOrdering(PassageTally.Order.TALLY);

                    for (int v = 1; v <= v11n.getLastVerse(bb, c); v++) {
                        Verse find = new Verse(v11n, bb, c, v);
                        BookData bdata = new BookData(book, find);
                        String text = OSISUtil.getPlainText(bdata.getOsisFragment());
                        PassageTally temp = (PassageTally) book.find(text);
                        temp.setOrdering(PassageTally.Order.TALLY);
                        total.addAll(temp);
                    }

                    int ff = v11n.getLastVerse(bb, c);
                    VerseRange base = new VerseRange(v11n, new Verse(v11n, bb, c, 1), new Verse(v11n, bb, c, ff));

                    total.remove(range);
                    total.trimVerses(LINKS_PER_CHAPTER);
                    scrunchTally(total);

                    Iterator iter = total.iterator();
                    while (iter.hasNext()) {
                        Key key = (Key) iter.next();
                        Verse link = (Verse) key;
                        VerseRange chap = new VerseRange(v11n, link, new Verse(v11n, link.getBook(), link.getChapter(), v11n.getLastVerse(link.getBook(), link.getChapter())));
                        Element el = new Element("link");
                        el.setAttribute("book", link.getBook().toString());
                        el.setAttribute("chapter", "" + link.getChapter());
                        el.setAttribute("name", chap.getName());
                        el.setAttribute("rating", "" + total.getIndexOf(link));
                        ec.addContent(el);

                        dbout.println(base.getName() + "," + base.getStart().getBook() + "," + base.getStart().getChapter() + "," + chap.getName() + ","
                                + link.getBook() + "," + link.getChapter() + "," + total.getIndexOf(link));

                        for (int j = 1; j <= v11n.getBookCount(); j++) {
                        	    BibleBook tb = v11n.getBook(j);
                            for (int tc = 0; tc < v11n.getLastChapter(tb); tc++) {
                                Verse t = new Verse(v11n, tb, tc, 1);
                                total.getIndexOf(t);
                            }
                        }
                    }
                }
            }

            xlout.close();
            dbout.close();

            PrintWriter xmlout = new PrintWriter(new FileOutputStream("c:\\links.xml"));
            Document doc = new Document(links);
            XMLOutputter output = new XMLOutputter(Format.getPrettyFormat());
            output.output(doc, xmlout);
            xmlout.close();
        } catch (Exception ex) {
            ex.printStackTrace(System.err);
        }
    }

    private void scrunchTally(PassageTally tally) {
        for (int b = 1; b < v11n.getBookCount(); b++) {
        	    BibleBook bb = v11n.getBook(b);
            for (int c = 1; c < v11n.getLastChapter(bb); c++) {
                Verse start = new Verse(v11n, bb, c, 1);
                Verse end = new Verse(v11n, bb, c, v11n.getLastVerse(bb, c));
                VerseRange chapter = new VerseRange(v11n, start, end);

                int chaptotal = 0;

                for (int v = 1; v < v11n.getLastVerse(bb, c); v++) {
                    chaptotal += tally.getTallyOf(new Verse(v11n, bb, c, v));
                }

                tally.remove(chapter);
                tally.add(start, chaptotal);

                if (chaptotal > PassageTally.MAX_TALLY) {
                    System.out.println("truncated chaptotal: " + chaptotal);
                }
            }
        }
    }

    public static final int LINKS_PER_CHAPTER = 200;

    Versification v11n;
    /*
     * // Remove the original wherever it was tally.remove(verse);
     * 
     * // Create the links for the tally links[index] = new
     * Link[LINKS_PER_VERSE]; for (int i=0; i<LINKS_PER_VERSE; i++) { try {
     * Verse loop = tally.getVerseAt(i); int strength = tally.getTallyOf(loop);
     * 
     * links[index][i] = new Link(loop.getOrdinal(), strength); } catch
     * (ArrayIndexOutOfBoundsException ex) { links[index][i] = new
     * Link(verse.getOrdinal(), 0); } }
     * 
     * return links[index];
     */
}
