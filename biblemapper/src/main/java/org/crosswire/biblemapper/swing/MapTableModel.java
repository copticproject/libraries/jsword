/**
 * Distribution License:
 * JSword is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, version 2 as published by
 * the Free Software Foundation. This program is distributed in the hope
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * The License is available on the internet at:
 *       http://www.gnu.org/copyleft/gpl.html
 * or by writing to:
 *      Free Software Foundation, Inc.
 *      59 Temple Place - Suite 330
 *      Boston, MA 02111-1307, USA
 *
 * Copyright: 2005
 *     The copyright to this program is held by it's authors.
 *
 * ID: $Id$
 */
package org.crosswire.biblemapper.swing;

import javax.swing.SwingUtilities;
import javax.swing.table.AbstractTableModel;

import org.crosswire.biblemapper.model.Map;
import org.crosswire.biblemapper.model.MapEvent;
import org.crosswire.biblemapper.model.MapListener;
import org.crosswire.jsword.passage.Verse;
import org.crosswire.jsword.passage.VerseRange;
import org.crosswire.jsword.versification.BibleBook;
import org.crosswire.jsword.versification.Versification;

/**
 * A MapTableModel takes an underlying map and represents it as a TableModel.
 * 
 * @see gnu.gpl.License for license details.<br>
 *      The copyright to this program is held by it's authors.
 * @author Joe Walker [joe at eireneh dot com]
 */
public class MapTableModel extends AbstractTableModel {
    /**
     * Basic constructor
     */
    public MapTableModel(Map map) {
        setMap(map);
    }

    /**
     * Basic constructor
     */
    public MapTableModel() {
        setMap(null);
    }

    /**
     * Setup a new map to view
     * 
     * @param map
     *            The new map to model
     */
    public void setMap(Map map) {
        if (map != null) {
            map.removeMapListener(cml);
        }

        this.map = map;

        if (map != null) {
            map.addMapListener(cml);
            cols = map.getDimensions() + 3;
        } else {
            cols = 1;
        }

        fireTableDataChanged();
    }

    /**
     * Get the map being viewed
     * 
     * @return The current map
     */
    public Map getMap() {
        return map;
    }

    /**
     * Returns the number of records in the map
     * 
     * @return the number or rows in the model
     * @see #getColumnCount()
     */
    public int getRowCount() {
        Versification v11n = map.getBible().getVersification();
        return v11n.getChapterCount();
    }

    /**
     * Returns the number of columns in the map
     * 
     * @return the number or columns in the model
     * @see #getRowCount()
     */
    public int getColumnCount() {
        return cols;
    }

    /**
     * Returns the name of the column at <i>columnIndex</i>. This is used to
     * initialize the table's column header name. Note, this name does not need
     * to be unique. Two columns on a table can have the same name.
     * 
     * @param col
     *            The index of column
     * @return the name of the column
     */
    @Override
    public String getColumnName(int col) {
        switch (col) {
        case 0:
            return "Name";
        case 1:
            return "Book";
        case 2:
            return "Chapter";
        case 3:
            return "X Position";
        case 4:
            return "Y Position";
        case 5:
            return "Z Position";
        default:
            return "Dimension " + (col - 3);
        }
    }

    /**
     * Returns the lowest common denominator Class in the column. This is used
     * by the table to set up a default renderer and editor for the column.
     * 
     * @return the common ancestor class of the object values in the model.
     */
    @Override
    public Class getColumnClass(int col) {
        switch (col) {
        case 0:
            return VerseRange.class;
        case 1:
        case 2:
            return Integer.class;
        default:
            return Float.class;
        }
    }

    /**
     * Returns true if the cell at <I>row</I> and <I>col</I> is editable.
     * Otherwise, setValueAt() on the cell will not change the value of that
     * cell.
     * 
     * @param row
     *            The row whose value is to be looked up
     * @param col
     *            The column whose value is to be looked up
     * @return rue if the cell is editable.
     * @see #setValueAt(Object, int, int)
     */
    @Override
    public boolean isCellEditable(int row, int col) {
        return col > 2;
    }

    /**
     * Returns an attribute value for the cell at <I>col</I> and <I>row</I>.
     * 
     * @param row
     *            The row whose value is to be looked up
     * @param col
     *            The column whose value is to be looked up
     * @return The value Object at the specified cell
     */
    public Object getValueAt(int row, int col) {
        Versification v11n = map.getBible().getVersification();
        // convert the row number (=verse ordinal - 1)
        // to a book and chapter number.
        int count = row + 1;
        int b = 1;
        int c = 1;
        while (b <= v11n.getBookCount()) {
            BibleBook bb = v11n.getBook(b);
            if (count <= v11n.getLastChapter(bb)) {
                c = count;
                break;
            }

            count -= v11n.getLastChapter(bb);
            b++;
        }
        BibleBook bb = v11n.getBook(b);
        switch (col) {
        case 0:
            VerseRange vr = new VerseRange(v11n, new Verse(v11n, bb, c, 1), new Verse(v11n, bb, c, v11n.getLastVerse(bb, c)));
            return vr;
        case 1:
            return Integer.valueOf(b);
        case 2:
            return Integer.valueOf(c);
        default:
            float f = map.getPositionDimension(bb, c, col - 3);
            return Float.valueOf(f);
        }
    }

    /**
     * Sets an attribute value for the record in the cell at <I>col</I> and
     * <I>row</I>. <I>val</I> is the new value.
     * 
     * @param val
     *            The new value
     * @param row
     *            The row whose value is to be changed
     * @param col
     *            The column whose value is to be changed
     * @see #getValueAt(int, int)
     * @see #isCellEditable(int, int)
     */
    @Override
    public void setValueAt(Object val, int row, int col) {
        Versification v11n = map.getBible().getVersification();

        float f = Float.valueOf(val.toString()).floatValue();
        Verse v = new Verse(v11n, row + 1);

        if (col > 2) {
            map.setPositionDimension(v.getBook(), v.getChapter(), col - 1, f);
        }
    }

    /**
     * The map that we are viewing
     */
    private Map map;

    /**
     * The number of columns
     */
    private static int cols;

    /**
     * The map listener
     */
    private CustomMapListener cml = new CustomMapListener();

    /**
     * Serialization ID
     */
    private static final long serialVersionUID = 3906649695802963768L;

    /**
     * Sync the map and the table
     */
    class CustomMapListener implements MapListener {
        /**
         * This method is called to indicate that a node on the map has moved.
         * 
         * @param ev
         *            Describes the change
         */
        public void mapChanged(final MapEvent ev) {
            if (!SwingUtilities.isEventDispatchThread()) {
                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        mapChanged(ev);
                    }
                });

                return;
            }

            Versification v11n = map.getBible().getVersification();
            BibleBook book = ev.getChangedBook();
            int chapter = ev.getChangedChapter();
            Verse v = new Verse(v11n, book, chapter, 1);
            int ord = v11n.getOrdinal(v);
            int row = ord - 1;

            fireTableCellUpdated(row, 2);
            fireTableCellUpdated(row, 3);
        }

        /**
         * This method is called to indicate that the whole map has changed
         * 
         * @param ev
         *            Describes the change
         */
        public void mapRewritten(final MapEvent ev) {
            if (!SwingUtilities.isEventDispatchThread()) {
                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        mapRewritten(ev);
                    }
                });

                return;
            }

            fireTableDataChanged();
        }
    }
}
