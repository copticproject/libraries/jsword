package org.crosswire.common.aqua;

import com.apple.eawt.AboutHandler;
import com.apple.eawt.AppEvent;
import com.apple.eawt.AppEvent.AboutEvent;
import com.apple.eawt.AppEvent.PreferencesEvent;
import com.apple.eawt.AppEvent.QuitEvent;
import com.apple.eawt.AppEventListener;
import com.apple.eawt.Application;
import com.apple.eawt.PreferencesHandler;
import com.apple.eawt.QuitHandler;
import com.apple.eawt.QuitResponse;
import com.apple.eawt.QuitStrategy;

import org.crosswire.common.swing.Actionable;

@SuppressWarnings("javadoc")
public class OSXAdapter implements AppEventListener, AboutHandler, PreferencesHandler, QuitHandler{

    private OSXAdapter(Actionable actionable, String aboutAction, String prefAction, String quitAction) {
        this.actionable = actionable;
        this.aboutAction = aboutAction;
        this.prefAction = prefAction;
        this.quitAction = quitAction;
    }

    // The main entry-point for this functionality. This is the only method
    // that needs to be called at runtime, and it can easily be done using
    // reflection (see MyApp.java)
    /**
     * Register the application so that About and Quit on the Application menu
     * are hooked to the applications About and Quit choices.
     */
    public static void registerMacOSXApplication(Actionable actionable, String aboutAction, String prefAction, String quitAction) {
        if (theApplication == null) {
            theApplication = Application.getApplication();
        }

        if (theAdapter == null) {
            theAdapter = new OSXAdapter(actionable, aboutAction, prefAction, quitAction);
        }
        theApplication.setPreferencesHandler(theAdapter);
        theApplication.setAboutHandler(theAdapter);
        theApplication.setQuitHandler(theAdapter);
        theApplication.setQuitStrategy(QuitStrategy.CLOSE_ALL_WINDOWS);
        theApplication.addAppEventListener(theAdapter);
    }

    /**
     * Enables the Preferences menu item in the application menu.
     */
    public static void enablePrefs(boolean enabled) {
        if (theApplication == null) {
            theApplication = Application.getApplication();
        }
        theApplication.setEnabledPreferencesMenu(enabled);
    }

    /* (non-Javadoc)
     * @see com.apple.eawt.QuitHandler#handleQuitRequestWith(com.apple.eawt.AppEvent.QuitEvent, com.apple.eawt.QuitResponse)
     */
    public void handleQuitRequestWith(QuitEvent ae, QuitResponse arg1) {
        /*
         * You MUST setHandled(false) if you want to delay or cancel the quit.
         * This is important for cross-platform development -- have a universal
         * quit routine that chooses whether or not to quit, so the
         * functionality is identical on all platforms. This example simply
         * cancels the AppleEvent-based quit and defers to that universal
         * method.
         */
        handle(quitAction, ae, true);
    }

    /* (non-Javadoc)
     * @see com.apple.eawt.PreferencesHandler#handlePreferences(com.apple.eawt.AppEvent.PreferencesEvent)
     */
    public void handlePreferences(PreferencesEvent ae) {
        handle(prefAction, ae, true);
    }

    /* (non-Javadoc)
     * @see com.apple.eawt.AboutHandler#handleAbout(com.apple.eawt.AppEvent.AboutEvent)
     */
    public void handleAbout(final AboutEvent ae) {
        handle(aboutAction, ae, true);
    }

    /**
     * @param action The Action to be performed
     * @param ae The Application Event
     * @param handledState The state for handling
     */
    private void handle(String action, AppEvent ae, boolean handledState) {
        if (actionable != null) {
            actionable.actionPerformed(action);
        } else {
            throw new IllegalStateException("handleQuit: MyApp instance detached from listener");
        }
    }

    /** This adapter is a singleton */
    private static OSXAdapter theAdapter;

    /** The MacOSX notion of the application */
    private static Application theApplication;

    /** The application providing about, preferences and quit. */
    private Actionable actionable;

    /** The application's About action */
    private String aboutAction;

    /** The application's Preferences action */
    private String prefAction;

    /** The application's Quit action */
    private String quitAction;

}
